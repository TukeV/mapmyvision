using UnityEngine;
using UnityEngine.UI;

namespace MapMyVision.Assets.Scripts
{
    /// <summary>
    /// This file includes a lot of variables and constants program uses such as instructions and settings.
    /// The variable settings are stored into Unity's PlyaerPrefs and they are loaded here, so they can be accessed when needed.
    /// </summary>
    public class STRINGS
    {
        /// <summary>Text show on top of the box before the test starts.</summary>
        public const string LOOK2START = "Look at the box\nto start the test.";

        /// <summary>Text show on top of the box to tell user that test has started.</summary>
        public const string START = "START";
        
        /// <summary>Instructions for appearing spheres test.</summary>
        public const string CLICK_WHEN_SEE = "Press any button when\nsphere appears.";

        /// <summary>Instructions for disappearing spheres test.</summary>
        public const string CLICK_WHEN_NO_SEE = "Press any button when\nsphere disappears.";

        /// <summary>Message shown on top of the box after test has completed, but the user has not yet answered to save prompt.</summary>
        public const string DONE = "Done.";

        /// <summary>Error message to tell user that app does not have filesystem access.</summary>
        public const string NOPERMISSION = "The App does not have permission to save.";

        /// <summary>Message shown to the user when they did not want to save the results.</summary>
        public const string NOTSAVED = "The Test result were not saved.";

        /// <summary>Message shown to the user after test results were succesfully saved.</summary>
        public const string SAVED = "Test result saved.";
    }

    /// <summary>NumberSettings is a static class which used to pass test settings to the testing scene.</summary>
    /// <remark>Apparently it is not possible start scenes with certain parameters, but those parameters can still passed
    ///  by creating static classes. Hence this class..</remarks>
    public class NumberSettings{
        
        /// <summary><c>Testlength</c> tells how long the test will be, ie. how many samples there will be.</summary>
        public static int TestLength {get; set;}

        /// <summary><c>Timer</c> is the time how long user has to look to the box before test actually starts.</summary>
        /// <remarks>I added this variable because I thought it would be something I would like to adjust, 
        /// but at the time of writing I don't believe there is any need to tinker with this variable in menus or settings.</remarks>
        public static float Timer {get; set;}
        
        /// <summary>Speed of the sphere in degrees/second.</summary>
        /// <remarks>In the older version the speed was calculated from it's time to travel from the starting point to it's destination.</remarks>
        public static float Speed {get; set;}

        /// <summary>
        /// <c>SpawnAngle</c> is either the angle where spheres spawn (appearing spheres test) or disappear (disappearing spheres test).
        /// <seealso cref="NumberSettings.Speed"/>
        /// <seealso cref="NumberSettings.Direction"/>
        /// </summary>
        public static float SpawnAngle {get; set;}

        /// <summary><c>Direction</c> tells if the spheres are moving towards (Appearing spheres) or away (disappearing spheres) from the central point.</summary>
        /// <value>1 means that spheres are moving towards the central point. 0 means the spheres are moving away from the central point.</value>
        /// <example>When <c>Direction</c> is 1, the user has to click when the sphere appears in their vision and vica versa.</example>
        public static int Direction {get; set;}

        /// <summary>Sets the size of the sphere. Default value is 1.</summary>
        public static float Size {get; set;}

        /// <summary>Sets the default values to the <c>NumberSettings</c></summary>
        /// <remark>The default listed here do not actually do anything, because values are always copied from the <c>AppearTestSettings</c> or
        /// <c>DisappearTestSettings</c> before starting the Sphere Test Scene.</remark>   
        static NumberSettings(){
            TestLength = 100;
            Timer = 3f;
            SpawnAngle = 45f;
            Speed = 5f;
            Direction = 1;
            Size = 0.2f;
        }
    }

    /// <summary>
    /// Class which holds the various colors the cube uses.
    /// </summary>
    public class CubeColors{
        /// <summary>Color when nothing is happening.</summary>
        static public Color Idle = Color.gray;

        /// <summary>Color during the countdown before the test has started.</summary>
        static public Color Countdown = Color.red;
        
        /// <summary>Color during the test session</summary>
        static public Color Testing = Color.blue;
        
        /// <summary>Color used in error cases.</summary>
        static public Color Null = Color.magenta;
        
        /// <summary>Color used when app is saving the test results.</summary>
        static public Color Saving = Color.gray;
        
        /// <summary>Color when test has finished and results have been saved.</summary>
        static public Color Finished = Color.green;
        
        /// <summary>Color when test results were not saved.</summary>
        static public Color NotSaved = Color.cyan;
    }
}