﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MapMyVision.Assets.Scripts;
using MapMyVision.Assets.Scripts.AreaTestSceneScripts;

public class AreaDraw : MonoBehaviour
{
    Material cubeMaterial;

    // Start is called before the first frame update
    void Start()
    {
        cubeMaterial = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetUp(OVRInput.Button.Back)){
            cubeMaterial.SetColor("_Color", Color.red);
        }else if(OVRInput.GetUp(OVRInput.Button.One)){
            cubeMaterial.SetColor("_Color", Color.blue);
        }else if(OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger)){
            cubeMaterial.SetColor("_Color", Color.green);
        }
    }
}
