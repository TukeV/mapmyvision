﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerDot : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject Cursor;
    Plane drawArea = new Plane(new Vector3(0f, 0f, 19f), new Vector3(10f, 0f, 19f), new Vector3(0f, 10f, 19f));

    LineRenderer lineRenderer;

    void Start()
    {
        Cursor = GameObject.Find("Cursor");
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(gameObject.transform.position, gameObject.transform.forward);
        float enter = .0f;
        if(drawArea.Raycast(ray, out enter)){
            Vector3 point = ray.GetPoint(enter);
            Cursor.transform.position = point;
        }
    }
}
