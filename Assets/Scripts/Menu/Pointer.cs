﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour
{
    /// <summary>
    /// A simple pointer for the menus. Draws a simple white line from the controller to forward.
    /// </summary>
    /// <value><c>lineRenderer</c> is the the line starting from the pointer and it's 5f unity units long.</value>
    /// <value><c>RayWidth</c> is the thickness of the line.</value>
    /// <value><c>RayLength</c> is the length of the pointer.</value>
    LineRenderer lineRenderer;
    // Start is called before the first frame update
    public float RayWidth = .01f;
    public float RayLength = 5f;

    /// <summary>
    /// Initializes the lineRenderer and sets it's color to white. 
    /// </summary>
    void Start()
    {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        if(lineRenderer != null){
            lineRenderer.material.color = Color.white;
        }
        lineRenderer.startWidth = RayWidth;
        lineRenderer.endWidth = RayWidth;
    }

    /// <summary>
    /// Updates the line renderer location.
    /// </summary>
    void Update()
    {
        if(lineRenderer != null){
            lineRenderer.SetPosition(0, gameObject.transform.position);
            lineRenderer.SetPosition(1, gameObject.transform.position + RayLength * gameObject.transform.forward);
        }
    }
}
