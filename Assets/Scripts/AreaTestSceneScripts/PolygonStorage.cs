using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MapMyVision.Assets.Scripts;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif


namespace MapMyVision.Assets.Scripts.AreaTestSceneScripts
{
    public class PolygonStorage
    {
        private class Polygon : List<PolarData> {};

        private List<Polygon> polygons = new List<Polygon>();
        private Polygon currentPolygon = new Polygon();

        public void StoreCurrentPolygon(){
            // Stores the current polygon and creates a new empty polygon
            polygons.Add(currentPolygon);
            currentPolygon = new Polygon();
        }

        public int CurrentPolygonSize(){
            // gets the size of the current polygon
            return currentPolygon.Count;
        }

        public void AddVertex(float angle, float radius, float fov){
            // Adds new vertex to the current Polygon
            PolarData datapoint = new PolarData(angle, radius, fov);
            currentPolygon.Add(datapoint);
        }

        public void RemoveLastVertex(){
            // Removes the previous vertex. If the polygon is empty, this does nothing.
            if(currentPolygon.Count > 0)
                currentPolygon.RemoveAt(currentPolygon.Count-1);
        }

        public bool writeToFile(string filename){
            try{
                if(!Directory.Exists(Application.persistentDataPath))
                    Directory.CreateDirectory(Application.persistentDataPath);
            }catch(Exception e){
                Debug.Log("Exception: "+ e.ToString());
                return false;
            }
            // Create file path
            string fullpath = Application.persistentDataPath + "/" + filename;
            using(StreamWriter sw = File.CreateText(fullpath)){
                sw.WriteLine("You forgot to write the function :/");
            }
            return true;
        }
    }
}