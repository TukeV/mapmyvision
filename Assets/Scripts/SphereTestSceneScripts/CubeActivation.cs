﻿using System.Collections;
using System.Collections.Generic;
using MapMyVision.Assets.Scripts;
using MapMyVision.Assets.Scripts.StateMachine;
using UnityEngine;
using UnityEngine.SceneManagement;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class CubeActivation : MonoBehaviour
{
    /// <summary>
    /// User interracts with the scene by either looking at, or looking away from the cube in the middle of the scene. The (assumend) eye contact between  user
    /// and the cube is what first starts the test and then keeps it going. User can take a break from the test by looking away from the cube. This why the
    /// statemachine controlling the scene is attached to the cube.
    /// 
    /// When the statemachine has been finished at the end of the scene, the cube activation will also draw the results on the screen.
    /// </summary>
    
    // The pointer line drawn from the oculus go controller
    GameObject pointer;

    /// <value><c>crosshair</c> is a small dot drawn in the center of the user's view point. It is a visual aid to tell user where he should
    /// look in order to start the test. It is only drawn when in the <c>IDLE</c> and <c>COUNTDOWN</c> states.</value>
    GameObject crosshair;

    /// <value><c>cubeMaterial</c>Handle to the cube's color</value>
    Material cubeMaterial;

    State sceneState = State.IDLE;

    /// <value><c>currentState</c> holds the current State.</value>
    StateHandler currentState;

    /// <value>
    /// <c>nullState</c>, <c>idleState</c>, <c>countDownState</c>, <c>testState</c><c>saveState</c> are the handles for the different states
    /// of the state machine. The transitions between the states are handled inside the <c>StateHandler.getNextState()<c> method.
    /// </value>
    NullState nullState;
    IdleState idleState;
    CountdownState countdownState;
    TestState testState;
    SphereDataStorage dataStore;
    public SaveHandler saveState;

    /// <value><c>VertexCount</c> is the number of vertices in the polar coordinate axis drawn at the end of the test.</value>
    private readonly int VertexCount = 361;

    /// <value><c>dialog</c> is used to show prompt for external storage access if the app is ran in the Oculus Go.</value>
    GameObject dialog;
    // Start is called before the first frame update
    void Start()
    {

// Request permission to the external storage, if the app does not have it yet.
#if PLATFORM_ANDROID
        if(!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead) || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)){
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            dialog = new GameObject();
        }
#endif

        crosshair = GameObject.Find("Crosshair");
        pointer = GameObject.Find("PointerLine");
        cubeMaterial = GetComponent<Renderer>().material;
        dataStore = new SphereDataStorage();
        // Statemachine Classes
        testState = new TestState(gameObject, dataStore);
        nullState = new NullState(gameObject);
        idleState = new IdleState(gameObject, testState);
        countdownState = new CountdownState(gameObject);
        saveState = new SaveHandler(gameObject, dataStore);
        // start from the idle state
        currentState = idleState;
    }

    void OnGUI()
    {
// Destroy the GUI after it's used.
#if PLATFORM_ANDROID
        if(dialog != null){
            Destroy(dialog);
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {
        // Next state is determined in the current state.
        sceneState = currentState.getNextState();
        switch (sceneState)
        {
            case State.IDLE:
                // App starts from the idle state and returns here if user loses eye contact to the cube during the test.
                pointer.SetActive(true);
                crosshair.SetActive(true);
                cubeMaterial.SetColor("_Color", CubeColors.Idle);
                currentState = idleState;
                break;
            case State.COUNTDOWN:
                // The countdown state lasts for few seconds. Test will start if the user keeps eye contact during the countdown.
                // Countdown length is determined in shared pereferences.
                pointer.SetActive(false);
                cubeMaterial.SetColor("_Color", CubeColors.Countdown);
                currentState = countdownState;
                break;
            case State.TEST:
                // Testing state. This lasts until user has either completed the test or they lose their direct eye contanct to the cube.
                pointer.SetActive(false);
                crosshair.SetActive(false);
                cubeMaterial.SetColor("_Color", CubeColors.Testing);
                currentState = testState;
                break;
            case State.SAVE:
                // After the test, the scene prompts user to either save or discard the test result.
                pointer.SetActive(true);
                crosshair.SetActive(true);
                cubeMaterial.SetColor("_Color", CubeColors.Saving);
                currentState = saveState;
                break;
            case State.FINISHED:
                // This is the end state of the test. This case is only visited once after user exits the SAVE state.
                // Then the state machine moves to the null state.
                // THOUGHT: Maybe all of this should be in the finishedState.updateState() handler.
                pointer.SetActive(true);
                crosshair.SetActive(true);
                ResolveCubeFinalState();
                ShowResults();
                currentState = nullState;
                break;
            default:
                // The default state in case of errors or if the user has finished the test.
                currentState = nullState;
                break;
        }
        // The main action of the state is handled here.
        currentState.updateState();
        // Return to main menu, if "Back" button is pressed.
        if (OVRInput.GetUp(OVRInput.Button.Back)){
            dataStore.clear();
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }

    /// <summary>
    /// This method determines the cube's color and info text after state machine has moved from
    /// <c>State.SAVE</c> to the <c>State.FINISHED</c>.
    /// </summary>
    void ResolveCubeFinalState(){
        TextMesh textMesh = GameObject.Find("InfoText").GetComponent<TextMesh>() as TextMesh;
        if(!saveState.UserPermission){
            // App does not have permission
            textMesh.text = STRINGS.NOPERMISSION;
            cubeMaterial.SetColor("_Color", CubeColors.Null);
        }else if(!saveState.Save){
            // User did not want to save the results
            textMesh.text = STRINGS.NOTSAVED;
            cubeMaterial.SetColor("_Color", CubeColors.NotSaved);
        }else if(!saveState.Success){
            // App was not able to save the results.
            textMesh.text = STRINGS.NOTSAVED;
            cubeMaterial.SetColor("_Color", CubeColors.Null);
        }else{
            // Results were saved succesfully.
            textMesh.text = STRINGS.SAVED;
            cubeMaterial.SetColor("_Color", CubeColors.Finished);
        }
    }

    /// <summary>
    /// Draws polar coordinate grid and locations where the spheres were when user pressed the button. 
    /// </summary>
    void ShowResults(){
        float z = gameObject.transform.position.z;
        // Draw the sphere locations
        foreach(var measurement in dataStore.Measurements){
            float x = measurement.radius * Mathf.Cos(measurement.polar);
            float y = measurement.radius * Mathf.Sin(measurement.polar);
            GameObject sphere = Object.Instantiate(Resources.Load("WhiteSphere"), new Vector3(x,y,z), Quaternion.identity) as GameObject;
        }

        // Draw Polar Grid
        foreach(float fovDegree in new float[] {5f, 10f, 15f, 20f, 25f, 30f, 35f, 40f, 45f, 50f}){
            GameObject axisContainer = new GameObject();
            axisContainer.transform.position = gameObject.transform.position;
            float fovRadian = Mathf.Deg2Rad * fovDegree;
            // Convert the FOV of the user into radial coordinate axis.
            float tickRadius = z * Mathf.Tan(fovRadian);
            GameObject lineContainer = CreateCircle(tickRadius);
            lineContainer.transform.parent = axisContainer.transform;
            GameObject tickText = CreateTick(tickRadius, fovDegree);
            tickText.transform.parent = axisContainer.transform;
        }
    }

    /// <summary>
    /// Function which draws a circle for the polar coordinate grid.
    /// </summary>
    /// <param name="radius">Radius of the circle</param>
    /// <returns>The gameobject, where the resulting linerenderer is attached</returns>
    private GameObject CreateCircle(float radius){
        float z = gameObject.transform.position.z;
        GameObject lineContainer = new GameObject();
        lineContainer.transform.position = gameObject.transform.position;
        LineRenderer line = lineContainer.AddComponent<LineRenderer>();
        Vector3[] vertices = new Vector3[VertexCount];
        for(int i = 0; i < VertexCount; ++i){
            // Calculate the vertex locations across the circle.
            float angle = Mathf.Deg2Rad * i;
            float x = radius * Mathf.Cos(angle);
            float y = radius * Mathf.Sin(angle);
            vertices[i] = new Vector3(x, y, z);
        }
        line.positionCount = VertexCount;
        line.SetPositions(vertices);
        line.startWidth = .1f;
        line.endWidth = .1f;
        line.material.color = Color.white;
        return lineContainer;
    }

    /// <summary>
    /// Draws a tick label for the polar grid
    /// </summary>
    /// <param name="radius">Radious of the circle, where the tick is going to be drawn</param>
    /// <param name="fov">The shown fov value. The ticks and circles are drawn using Unity coordinates, but the label will show
    /// the FOV angle of the user.</param>
    /// <returns>tickText gameObject</returns>
    private GameObject CreateTick(float radius, float fov){
        GameObject tickText = new GameObject();
        Vector3 textPosition = gameObject.transform.position;
        textPosition.x += radius + .1f;
        tickText.transform.position += textPosition;
        var textmesh = tickText.AddComponent<TextMesh>();
        textmesh.text = $"{fov}°";
        textmesh.characterSize = .5f;
        textmesh.alignment = TextAlignment.Center;
        return tickText;
    }
}
