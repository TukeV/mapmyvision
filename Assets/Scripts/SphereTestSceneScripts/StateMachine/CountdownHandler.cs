using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapMyVision.Assets.Scripts.StateMachine
{
    /// <summary>
    /// State which tracks the countdown. If user keeps looking at the cube the whole countdown, then the state machine will proceed
    /// to the test state. If the user stops looking at the cube, the machine will return to the idle state.
    /// </summary>
    public class CountdownState : StateHandler
    {
        TextMesh textMesh;
        float timer = NumberSettings.Timer;
        public CountdownState(GameObject gameObject) : base(gameObject)
        {
            textMesh = gameObject.transform.Find("InfoText").gameObject.GetComponent<TextMesh>() as TextMesh;
        }

        /// <summary>
        /// Either starts the test, keeps the current state or returns to the idle state depending on if user is looking at the cube or 
        /// how long they have been doing so.
        /// </summary>
        /// <returns>
        /// <c>State.IDLE</c> if user is not looking at the cube. 
        /// <c>State.COUNTDOWN</c> if countdown has not been finished yet.
        /// <c>State.TEST</c> if countdown has finished.
        /// </returns>
        public override State getNextState()
        {
            State nextState = State.COUNTDOWN;
            bool eyeContact = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, 100);
            if (!eyeContact){
                timer = NumberSettings.Timer;
                nextState = State.IDLE;
            }else if (timer <= -.5f){
                textMesh.text = "";
                timer = NumberSettings.Timer;
                nextState = State.TEST;
            }else{
                nextState = State.COUNTDOWN;
            }
            return nextState;
        }
        /// <summary>
        /// Updates the countdown text shown above the cube.
        /// </summary>
        /// <remark>
        /// While coroutines would have been more idiomatic way to handle the countdown, this state still needed a way to 
        /// interrupt the countdown when needed. I personally find this current implementation simple enough, while I do
        /// ackowledge that coroutines would have been more "cleaner" way to implement this.
        /// </remark>
        public override void updateState()
        {
            timer -= Time.deltaTime;
            if (timer >= 1f){
                textMesh.text = timer.ToString("N0");
            }else if (timer >= -.5f){
                textMesh.text = STRINGS.START;
            }else{
                textMesh.text = "";
            }
        }
    }
}