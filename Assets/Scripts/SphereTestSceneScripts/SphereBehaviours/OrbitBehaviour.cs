﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitBehaviour : MonoBehaviour
{
  /// <summary>
  /// A sphere, which orbits around the head of the user. Not used in the app at the time of writing.
  /// </summary>
    public Vector3 viewLocation;
    public float distance;
    public float angle = 0.0f;
    public float direction = 1.0f;
    public const float SPEED = 36f;

    // Start is called before the first frame update
    void Start()
    {
        viewLocation = Camera.main.transform.position;
        Vector3 sphereLocation = gameObject.transform.position;
        distance = Vector3.Distance(viewLocation, sphereLocation);
    }

    // Update is called once per frame
    void Update()
    {
        float deltaAngle = SPEED * Time.deltaTime * direction;
        angle += deltaAngle;
        transform.RotateAround(viewLocation, Vector3.up, deltaAngle);
        if(angle >= 90.0f){
          direction = -1.0f;
        }else if(angle <= -90.0f){
          direction = 1.0f;
        }
    }
}
