using MapMyVision.Assets.Scripts;
using UnityEngine;

namespace MapMyVision.Assets.Scripts.TestSceneScripts.StateMachine
{
    /// <summary>
    /// The state handler for the final state. There is literally nothing happening here.
    /// </summary>
    public class FinalState : StateHandler
    {
        public FinalState(GameObject gameObject) : base(gameObject){}
        public override State getNextState(){
            return State.FINISHED;
        }
        public override void updateState(){}
    }
}