using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif


namespace MapMyVision.Assets.Scripts
{
    /// <summary>
    /// The <c>PolarData</c> obtained from the test are stored here. The class also provides method to write results to a file if user wants to do so.
    /// </summary>
    /// <see cref="PolarData"/>  
    public class SphereDataStorage
    {

        private List<PolarData> m_measurements;
        public ReadOnlyCollection<PolarData> Measurements { 
            get{ return m_measurements.AsReadOnly(); }
        }

        public SphereDataStorage(){
            m_measurements = new List<PolarData>();
        }

        public void addMeasurement(float polarAngle, float radius, float fovAngle){
            m_measurements.Add(new PolarData(polarAngle, radius, fovAngle));
        }

        public void addMeasurement(PolarData m){
            m_measurements.Add(m);
        }

        public void clear(){
            m_measurements.Clear();
        }

        public void sortByPolarCordinate(){
            m_measurements.Sort((m1, m2) => m1.polar.CompareTo(m2.polar));
        }

        // TODO: Add size and brightness
        public bool writeToFile(string filename){
            // Make sure that the App has a directory for datafiles.
            try{
                if(!Directory.Exists(Application.persistentDataPath))
                    Directory.CreateDirectory(Application.persistentDataPath);
            }catch(Exception e){
                Debug.Log("Exception: "+ e.ToString());
                return false;
            }
            // Create file path
            string fullpath = Application.persistentDataPath + "/" + filename;
            // Write the Data
            using(StreamWriter sw = File.CreateText(fullpath)){
                /* Put Direction, Spawn Distance and Time2Travel to header.
                 Meaning of the Distance can and must be interpreted from direction value:
                 - If the spheres are going towards the center, then the distance means the spawn distance of the spheres.
                 - Speed is the sphere's speed in degrees per second.
                */
                sw.WriteLine(String.Format("// Used Settings: Direction: {0}, Distance: {1}, Speed: {2}, Size: {3}",
                        NumberSettings.Direction == 1 ? "In" : "Out",
                        NumberSettings.SpawnAngle, 
                        NumberSettings.Speed,
                        NumberSettings.Size)
                );
                sw.WriteLine("// PolarAngle, Radius, FoVAngle");
                sortByPolarCordinate();
                foreach(PolarData m in m_measurements){
                    sw.WriteLine($"{m.polar}, {m.radius}, {m.fov}");
                }
            }
            return true;
        }
    }
}