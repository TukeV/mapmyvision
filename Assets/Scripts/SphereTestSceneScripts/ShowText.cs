﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowText : MonoBehaviour
{
    private TextMesh textMesh;
    void Start(){
        // Get the text mesh component when the game object is instatiated.
        textMesh = gameObject.GetComponent<TextMesh>() as TextMesh;
    }

    // Update is called once per frame
    void Update(){}

    /// <summary>
    /// Method which can be used to make TextMesh to show a message for a while.
    /// </summary>
    /// <param name="text">Message to be shwon</param>
    /// <param name="time">Seconds before message is removed.</param>
    /// <remark>
    /// Because state machine objects are not MonoBehaviours, there needed to be another way to access unity's coroutines.
    /// Since this coroutine edits the text, I found it natural to attach this coroutine here.
    /// </remark>
    public void ShowTextForSeconds(string text, float time){
        StartCoroutine(ShowTextRoutine(text, time));
    }

    /// <summary>
    /// Coroutine, which shows the text for a while and then hides it.
    /// </summary>
    /// <param name="text">Message to be shown.</param>
    /// <param name="time">How many seconds message should show.</param>
    /// <returns></returns>
    IEnumerator ShowTextRoutine(string text, float time){
        textMesh.text = text;
        yield return new WaitForSeconds(time);
        textMesh.text = "";
    }
}
