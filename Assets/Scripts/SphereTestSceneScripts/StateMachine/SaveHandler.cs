using MapMyVision.Assets.Scripts;
using UnityEngine;
using System;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace MapMyVision.Assets.Scripts.StateMachine
{
    /// <summary>
    /// Handler for the save state, which will check if the app has permissions for save files 
    /// and the prompts user if they want to save their test results if user wants to save 
    /// the results, this class will then call the save method from <c>SphereDataStorage</c>
    /// </summary>
    /// <see cref=SaveDialogLstener/>
    public class SaveHandler : StateHandler
    {
        SphereDataStorage SphereDataStorage;
        bool completed = false;
        public bool Completed{ get { return completed; }}

        bool userpermission = false;
        public bool UserPermission{ get { return userpermission; }}
        bool save = false;
        public bool Save{ get { return save; }}
        bool success = false;
        public bool Success{ get {return success;}}

        bool activated = false;
        
        GameObject saveDialog;

        /// <summary>
        /// Initializes the instance and tries to searches for the save dialog in the scene.
        /// </summary>
        /// <param name="gameObject">parent game object, this is</param>
        /// <param name="tds"></param>
        /// <returns></returns>
        public SaveHandler(GameObject gameObject, SphereDataStorage tds) : base(gameObject){
            SphereDataStorage = tds;
            saveDialog = GameObject.Find("Canvas");
            // Prompt is hidden until the state machine enters this scene.
            saveDialog?.SetActive(false);
        }
        /// <summary>
        /// The save state will continue until user has either answered prompt or the state
        /// has determined that the app has no writing permission.
        /// </summary>
        /// <returns></returns>
        public override State getNextState()
        {
            return completed ? State.FINISHED : State.SAVE;
        }

        /// <summary>
        /// Checks if the user has writing permission or not. If user does not have the permission, 
        /// the scene is set to be ended. Otherwise the save prompt is brought up. 
        /// Note that the handler for the prompt (<c>SaveDialogListener</c>) 
        /// is initialized along with the scene.
        /// </summary>
        public override void updateState() {
            #if PLATFORM_ANDROID
            if(!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead) || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)){
                userpermission = false;
                completed = true;
            }else if(!completed){
                userpermission = true;
            }
            #else
            if(!completed){
                userpermission = true;
            }
            #endif
            if(userpermission && !activated){
                Debug.Log("Bring up the menu");
                GameObject.Find("SaveDialog").GetComponent<SaveDialogListener>().SaveState = this;
                saveDialog.SetActive(true);
                activated = true;
            }
        }
        /// <summary>
        /// User wants to save. The default filename will be the generated from the current time, 
        /// but in the future the code could easily be refactored to use different filenames.
        /// 
        /// After the record has been saved, the state will be marked as completed and the dialog will be set inactive again.
        /// </summary>
        public void DoSave(){
            Debug.Log("Do Save");
            save = true;
            string filename = DateTime.Now.ToString("yMMdd_HHmm") + "_spheres.csv";
            success = SphereDataStorage.writeToFile(filename);
            completed = true;
            saveDialog?.SetActive(false);
        }

        /// <summary>
        /// User did not want to save. The scene is marked completed and save dialog will be set to hidden again.
        /// </summary>
        public void DoNotSave(){
            Debug.Log("Do not Save");
            save = false;
            completed = true;
            saveDialog?.SetActive(false);
        }
    }
}