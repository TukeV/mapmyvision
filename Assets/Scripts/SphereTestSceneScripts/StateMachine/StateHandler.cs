using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapMyVision.Assets.Scripts
{
    /// <summary>
    /// Base class for the state machine's states. 
    /// </summary>
    /// <value>
    /// <c>State</c> is a enum which used in the switch case statement in the <c>CubeActivation</c> update.</value>
    /// 
   public enum State { IDLE, COUNTDOWN, TEST, SAVE, FINISHED, NULL};
   public abstract class StateHandler
    {
        public GameObject parent;
        protected StateHandler(GameObject parent)
        {
            this.parent = parent;
        }

        /// <summary>
        /// This function is called at the beginning of the update loop. With this function the state 
        /// can tell the main loop which state the state machine should proceed. It can return it's own
        /// state if it wants to continue it's task.
        /// </summary>
        /// <returns>The <c>State</c> where the state machine should move next.</returns>
        abstract public State getNextState();

        /// <summary>
        /// Here states can execute the actions they want to execute during this update cycle.
        /// </summary>
        abstract public void updateState();
    }

    public class NullState : StateHandler
    {
        /// <summary>
        /// An exmaple implementation of the <c>StateHandler</c> whicb does not do anything. It can be used as a default case 
        /// where state machine will proceed if any of the state's encounter an error.
        /// </summary>
        /// <returns></returns>
        public NullState(GameObject gameObject) : base(gameObject) { }

        /// <summary>
        /// Instructs state machine to stay in the this state.
        /// </summary>
        /// <returns><c>State.NULL</c>, ie. the state machine will loop in this state.</returns>
        public override State getNextState()
        {
            return State.NULL;
        }

        /// <summary>
        /// <c>NullState</c> does not do anything in it's update phase.
        /// </summary>
        public override void updateState() { }
    }
}