﻿using System.Collections;
using System.Collections.Generic;
using MapMyVision.Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;


#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class DrawLine : MonoBehaviour
{
    // Start is called before the first frame update
    LineRenderer lineRenderer;
    GameObject Cursor;
    List<Vector3> vertices;
    List<GameObject> spheres;
    GameObject saveDialog;
    bool drawing = true;
    void Start()
    {
        drawing = true;
        saveDialog = GameObject.Find("SaveDialog");
        saveDialog.SetActive(false);
        vertices = new List<Vector3>();
        spheres = new List<GameObject>();
        Cursor = GameObject.Find("Cursor");
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.material.SetColor("_Color", Color.magenta);
        lineRenderer.startWidth = .2f;
        lineRenderer.endWidth = .2f;
        lineRenderer.positionCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetUp(OVRInput.Button.Back)){
            // Prompt the save dialog
            drawing = false;
            saveDialog.SetActive(true);
        }else if(OVRInput.GetUp(OVRInput.Button.One) && drawing){
            // Remove the previous vertex
            vertices.RemoveAt(vertices.Count - 1);
            // update lineRenderer
            lineRenderer.positionCount = vertices.Count;
            lineRenderer.SetPositions(vertices.ToArray());
            // Remove sphere
            GameObject sphere = spheres[spheres.Count - 1];
            Destroy(sphere);
            spheres.RemoveAt(spheres.Count - 1);
        }else if(OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) && drawing){
            // Add vertice to storage
            Vector3 point = Cursor.gameObject.transform.position;
            vertices.Add(Cursor.gameObject.transform.position);
            // Update Line Renderer
            lineRenderer.positionCount = vertices.Count;
            lineRenderer.SetPositions(vertices.ToArray());
            // Add White sphere
            GameObject sphere = UnityEngine.Object.Instantiate(Resources.Load("WhiteSphere"), point, Quaternion.identity) as GameObject;
            spheres.Add(sphere);
        }
    }

    private PolarData ToPolarData(Vector3 location, Vector3 cubeLocation){
        float polar =  Mathf.Atan2(location.y, location.x);
        float radius = Vector3.Distance(location, cubeLocation);
        Vector3 cameraLocation = Camera.main.transform.position;
        Vector3 camera2sphere = cameraLocation - location;
        Vector3 camera2box = cameraLocation - cubeLocation;
        float fov = Vector3.Angle(camera2sphere, camera2box);
        return new PolarData(polar, radius, fov);
    }

    private bool write2file(string filename){
        // Make sure that the App has a directory for datafiles.
            try{
                if(!Directory.Exists(Application.persistentDataPath))
                    Directory.CreateDirectory(Application.persistentDataPath);
            }catch(Exception e){
                Debug.Log("Exception: "+ e.ToString());
                return false;
            }
            // Create file path
            string fullpath = Application.persistentDataPath + "/" + filename;
            
            // 
            Vector3 cubeLocation = GameObject.Find("Cube").transform.position;
            cubeLocation.z = Cursor.transform.position.z;
            // Write the Data
            using(StreamWriter sw = File.CreateText(fullpath)){
                sw.WriteLine("// In area test the datapoints were drawn by hand.");
                sw.WriteLine("// Format: PolarAngle, Radius, FoVAngle");
                foreach(Vector3 location in vertices){
                    PolarData polar = ToPolarData(location, cubeLocation);
                    sw.WriteLine($"{polar.polar}, {polar.radius}, {polar.fov}");
                }
            }
            return true;
    }

    public void Save(){
        string filename = DateTime.Now.ToString("yMMdd_HHmm") + "_area.csv";
        write2file(filename);
        ExitScene();
    }

    public void ExitScene(){
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}
