using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapMyVision.Assets.Scripts.StateMachine
{
    /// <summary>
    /// This is the handler for the starting state of the state machine. 
    /// It does not do anything special and the state machine will exit this state when user looks at the cube.
    /// </summary>
    public class IdleState : StateHandler
    {
        TextMesh textMesh;
        private TestState testState;
        public IdleState(GameObject gameObject, TestState testState) : base(gameObject)
        {
            this.testState = testState;
            textMesh = parent.transform.Find("InfoText").gameObject.GetComponent<TextMesh>() as TextMesh;
        }

        /// <summary>Checks if the user is looking at the cube or not</summary>
        /// <returns>
        /// Instruction to proceed to the <c>state.COUNTDOWN</C> if user is lookng at the cube.
        /// Otherwise stay in this state.
        /// </returns>
        public override State getNextState()
        {
            bool start = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, 100);
            return start ? State.COUNTDOWN : State.IDLE;
        }

        /// <summary>
        /// The instruction text howering above the cube will either display the test instructions 
        /// or test progress depending if user has started the test yet..
        /// </summary>
        public override void updateState()
        {
            if(testState.Progress == 0){
                textMesh.text = STRINGS.LOOK2START;
            }else{
                textMesh.text = $"Test progress: {testState.Progress} / {NumberSettings.TestLength}";
            }
        }
    }
}