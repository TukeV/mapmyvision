# Map My Vision

A virtual reality app to determine user's field of vision. App is rather useles for the people with normal vision due because most VR headsets have FOV closer to 100� which is nowhere near the FOV of an average person which is near 180�. However, for some visually impaired people (ie. people having tunnel vision) this app could prove helpful to determine the progession of the vision loss.

The app is made with Unity and it's targeted to Oculus Go VR headset.

## Testing peripherial vision

The peripherial vision test can be started by clicking *Start Test* in main menu. Then app loads the Test scene.

The test procedure is the following:

1. User is prompted to align the center of their vision (marked with dot) to the cube in front of them.
2. Cube changes color to red and starts a short countdown before the test begins. User has to keep their center of the vision in the cube.
3. The will turn to blue and the test is started. During the test white spheres are created to the edge of the vision, which will slowly wander to the center to the user's vision.
4. When user sees the sphere, they should press any button on Oculus Go controller. Pressing the button removes the sphere and creates a new sphere.
5. Test can be paused any time simply my moving the center of the vision away from the cube. The test can be resumed by re-focusing the crosshair to the cube.
6. After test is completed, the screen will prompt saving the test result.
7. The file is stored in the app's external storage directory.

There is also an option to reverse the direction of the spheres. If the direction of the spheres is set to *outwards*, the user is instead prompted to click any button whenever a sphere disappears from user's vision.

## Settings

The test properties can be configured from the settings menu.

- Direction where spheres are travelling ie. from edge to center or from middle to edge.
- Number of spheres used during the test session.
- Speed of the spheres or how long does it take for the sphere to move from it's spawn point to it's destination.
- If the travelling direction is set to inwards, then user can also set the initial distance (or angle) between the spawn point and the center point.

## Test Results

The app records the test results by determining the polar cordinates of the sphere: the app stores the polar angle and the radius between the cube and the sphere.
In addition, the app also calculates the angle between sphere, user and the cube.

The test results are stored in csv files, where first column is the polar angle in radians, second column is the range in in-game units and last column is the user's FOV angle in degrees.

## Future development

- Add a test, which could be used to map out the visible area. This is useful if the user's vision is holey or ragged ie. has way oore blind spots and areas compared to fully sighted person.

## Science and Warranty Disclaimer

Test has not been scientifically tested and it comes wtihout any warranties or liabilities. This app is made by an engineer without any experience in medical field and should be considered as such. Always consult certified medical professionals in matters of health, medication or therapy.
