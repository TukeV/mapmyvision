﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour
{
    /// <summary>
    /// The controller part of the main menu. The bindings to the <c>mainPanel</c>, <c>appearPanel</c> and <c>disappearPanel</c> are bound in the scene (Unity editor).
    /// </summary>
    /// <value><c>mainPanel</c> is the main menu panel and it has buttons for starting appear test, disappear test or exit the app.</value>
    /// <value><c>appearPanel</c> is a panel containing settings menu for appearing spheres test.</value>
    /// <value><c>disappearPanel</c>is a panel containing settings menu for disappearing spheres test.</value>
    public GameObject mainPanel;
    public GameObject appearPanel;
    public GameObject disappearPanel;

    /// <summary>
    /// At the beginning of the scene, the main menu is set visible.
    /// </summary>
    void Start(){
        ShowMainMenu();
    }

    /// <summary>
    /// Sets the main panel active and hides other panels by setting them inactive.
    /// </summary>
    public void ShowMainMenu(){
        mainPanel.SetActive(true);
        appearPanel.SetActive(false);
        disappearPanel.SetActive(false);
    }

    /// <summary>
    /// Sets the appearing spheres menu active and hides the main panel
    /// </summary>
    public void OnAppearClicked(){
        mainPanel.SetActive(false);
        appearPanel.SetActive(true);
    }

    /// <summary>
    /// Sets the disapearing spheres menu actuve and hides the main panel
    /// </summary>
    public void OnDisappearClicked(){
        mainPanel.SetActive(false);
        disappearPanel.SetActive(true);
    }

    /// <summary>
    /// Quits the app. This is bound to the exit button inside Unity editor.
    /// </summary>
    public void ExitApp(){
        Application.Quit();
    }

    /// <summary>
    /// Update only checks if the exit button on the controller is pressed. 
    /// The app will be closed, if the back button is clicked when main button is open.
    /// 
    /// </summary>
    void Update(){
        if (mainPanel.activeSelf && OVRInput.GetUp(OVRInput.Button.Back)){
            // Quit if this panel is open
            Application.Quit();
        }
    }
}
