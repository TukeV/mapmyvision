﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHandler : MonoBehaviour
{
    /// <summary>
    /// Monobehaviour for the controller in the test scene.
    /// </summary>
    /// <remarks>I'm not completely sure if actualy does anything...</remarks>
    GameObject textComponent;
    // Start is called before the first frame update
    void Start()
    {
        textComponent = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        if(OVRInput.GetUp(OVRInput.Button.Any)){
            textComponent.SetActive(!textComponent.activeInHierarchy);
        }
    }
}
