﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapMyVision.Assets.Scripts;

public class LineBehaviour : MonoBehaviour
{
    /// <summary>
    /// Behaviour for the spehre. This moves the spehere from <c>StartPoint</c> to <c>Destination</c> at the speed of <c>Speed</c>
    /// </summary>
    public float WorldSpeed;
    public float Distance;
    Vector3 Destination;
    Vector3 StartPoint;

    /// <summary>
    /// When the sphere is spawned, it will either move towards the cube or away from the cube.
    /// The direction is defined by the <c>NumberSettings.Direction</c> where 0 means that sphere will
    /// move from <c>cubeLocation</c> to the <c>Destination</c> and vica versa if direction is set to 1. 
    /// </summary>
    /// <remarks>This means that 0 is the setting for disappearing test and 1 is setting for appearing test.</remarks>
    /// <see cref="NumberSettings"/>
    void Start()
    {
        Vector3 cubeLocation = GameObject.Find("Cube").transform.position;

        // Get the sphere direction from the settings
        if(NumberSettings.Direction == 1){
            Destination = cubeLocation;
            StartPoint = transform.position;
        }else{
            Destination = transform.position;
            StartPoint = cubeLocation;
            transform.position = cubeLocation;
        }
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.SetColor("_Color", Color.white); // TODO: Color.white * brightness
        float size = NumberSettings.Size;
        transform.localScale = new Vector3(size, size, size);
        Distance = Vector3.Distance(StartPoint, Destination);
        // The speed in the number settings is in "deg/s", but in reality the sphere is moving in-game units.
        // Therefore, we need to convert the "deg/s" to "px/s", where px are the ingame coordinates.
        WorldSpeed = (NumberSettings.Speed * Distance) / NumberSettings.SpawnAngle;
    }

    /// <summary>Moves the sphere towards it's destination at the given speed.</summary>
    void Update()
    {
        float step = WorldSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, Destination, step);
    }
}
