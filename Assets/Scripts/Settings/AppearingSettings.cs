﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MapMyVision.Assets.Scripts;
using UnityEngine.UI;
using System;

/// <summary>
/// This class is used to control the menu which opens when user wants to start the test, 
/// where they need to click when sphere appears in their vision.
/// </summary>
public class AppearingSettings : MonoBehaviour
{
    /// <summary>
    /// These UI elements are created in the <c>MainMenu</c> scene and they are binded to these variables in the Unity Editor.
    /// </summary>
    public GameObject mainPanel;
    public GameObject appearPanel;
    
    public Slider LengthSlider;
    public Text LengthValueText;

    public Slider SpeedSlider;
    public Text SpeedValueText;

    public Slider AngleSlider;
    public Text AngleValueText;

    public Slider SizeSlider;
    public Text SizeValueText;

    void Start()
    {
        // Set UI to it's default state when the scene begins.
        SetDefaults();
    }

    /// <summary>
    /// Fetches the default values from <c>PlayerPrefs</c> storage and set's slider's to their values.
    /// </summary>
    // TODO: Sphere brightness & Sphere size
    void SetDefaults(){
        float testLength = (float) PlayerPrefs.GetInt("AppearTestLength", 100);
        float speed = PlayerPrefs.GetFloat("AppearSpeed", 1.2f);
        float spawnAngle = PlayerPrefs.GetFloat("SpawnAngle", 15f);
        float size = PlayerPrefs.GetFloat("SphereSize", 0.2f);

        LengthSlider.value = testLength;
        AngleSlider.value = spawnAngle;

        // While the speed and size values are floats, the slider actually uses
        // integers to make it appear snappy. 
        // The values on the slider ar 10 times larger than the actual values.
        SpeedSlider.value =  (int) Math.Round(speed * 10);
        SizeSlider.value =  (int) Math.Round(size * 10);
        // Sometimes labels fail to update, so we have to do it manually.
        UpdateLabels();
    }

    /// <summary>
    /// Updates all the labels by calling their update functions.
    /// </summary>
    void UpdateLabels(){
        OnAngleUpdated();
        OnSpeedUpdated();
        OnSizeUpdated();
        OnAngleUpdated();
    }

    /// <summary>
    /// Updates PlayerPreferences to the values given in parameters.
    /// </summary>
    /// <param name="testLength">How many samples the test will have.</param>
    /// <param name="speed">The speed of the sphere in the test</param>
    /// <param name="spawnAngle">How far away from the center the sphere will spawn.</param>
    /// <param name="size">Sphere Size</param>
    // TODO: Sphere brightness
    void UpdatePreferences(int testLength, float speed, float spawnAngle, float size){
        PlayerPrefs.SetInt("AppearTestLength", testLength);
        PlayerPrefs.SetFloat("AppearSpeed", speed);
        PlayerPrefs.SetFloat("SpawnAngle", spawnAngle);
        PlayerPrefs.SetFloat("SphereSize", size);
    }

    /// <summary>
    /// Sends the values to the Sphere Test Scene.
    /// </summary>
    /// <param name="testLength">How many sphere's there will be in the test.</param>
    /// <param name="speed">The speed of the sphere in degrees/second</param>
    /// <param name="spawnAngle">How far away from the center the sphere will spawn.</param>
    /// <param name="size">Sphere Size</param>
    /// <remark>Because these parameters cannot be sent to the test scene by other means, they are stored in the static class.</remark>
    // TODO: Sphere brightness
    void Copy2NumberSettings(int testLength, float speed, float spawnAngle, float size){
        NumberSettings.TestLength = testLength;
        NumberSettings.Speed = speed;
        NumberSettings.SpawnAngle = spawnAngle;
        NumberSettings.Size = size;
        NumberSettings.Direction = 1;
    }

    /// <summary>
    /// Callback which updates <c>LengthSliderText</c> when <c>LengthSlider</c> is updated.
    /// </summary>
    public void OnLengthUpdated(){
        LengthValueText.text = LengthSlider.value.ToString();
    }

    /// <summary>
    /// Callback which updates the <c>SpeedValueText</c> when <c>SpeedSlider</c> is updated.
    /// </summary>
    public void OnSpeedUpdated(){
        // Slider values are ten times larger so they need to be divided by 10.
        float value = (float) Math.Round(SpeedSlider.value * 0.1f, 1);
        SpeedValueText.text = $"{value} deg/s";
    }

    /// <summary>
    /// Callback which updates the <c>AngleValueText</c> when <c>AngleSlider</c> is updated.
    /// </summary>
    public void OnAngleUpdated(){
        AngleValueText.text = AngleSlider.value.ToString();
    }

    public void OnSizeUpdated(){
        // Slider values are ten times larger so they need to be divided by 10.
        float value = (float) Math.Round(SizeSlider.value * 0.1f, 1);
        SizeValueText.text = $"{value}";
    }

    public void OnBrightnessUpdated(){
        // TODO
    }

    /// <summary>
    /// Reads the settings from the UI, stores them to the <c>PlayerPrefs</c> before sending them 
    /// to the <c>NumberSettings</c> before starting the <c>SphereTestScene</c>.
    /// </summary>
    public void OnStartClicked(){
        int testLength = (int) LengthSlider.value;
        float spawnAngle = AngleSlider.value;
        // The speed and size slider values are 10 times larger, so they need to be divided by 10.
        float speed = (float) Math.Round(SpeedSlider.value * 0.1f, 1);
        float size = (float) Math.Round(SizeSlider.value * 0.1f, 1);
        // TODO: Sphere brightness
        UpdatePreferences(testLength, speed, spawnAngle, size);
        Copy2NumberSettings(testLength, speed, spawnAngle, size);
        SceneManager.LoadScene("SphereTestScene", LoadSceneMode.Single);
    }

    /// <summary>
    /// Resets the UI elements to their default values.
    /// </summary>
    public void OnDefaultClicked(){
        SetDefaults();
    }

    /// <summary>
    /// When Back button (the UI element) is clicked, the current panel is hidden and the main menu is set back to active.
    /// </summary>
    public void OnBackClicked(){
        mainPanel.SetActive(true);
        appearPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // If this panel is active and the back button on the controller is pressed, 
        // the effect will be the same as the UI button is pressed.
        if (appearPanel.activeSelf && OVRInput.GetUp(OVRInput.Button.Back)){
            // Disable this menu and re-open the main menu
            OnBackClicked();
        }
    }
}
