using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapMyVision.Assets.Scripts.StateMachine
{

    public class TestState : StateHandler
    {
        /**
        The TestState is the state during the Test or measuring session. The script will spawn and remove spheres and collect
        their locations to produce the test result.
        
        The test format is fairly simple: A sphere is spawned outside of the FOV of the user and it is moved slowly towards the center of the screen.
        When user sees the sphere, he/she is will press the button on the controller. When controller is pressed, the sphere's polar coordinates are
        stored and a new sphere is spawned.

        The spheres are initially spawned on a circle around the blue box (transform.position.z). The radius of this spawning circle is

        radius = z_bluebox * Tan(angle), where
        z_bluebox is the distance between user and the box.

        angle is the NumberSettings.SpawnAngle. User can edit this this constant from the settings menu.
        */
        
        // Keeps track on how many sphere user has clicked during the test, so that test can be resumed if user loses his/her focus.
        private int sphereCount;
        public int Progress { get{ return sphereCount; } }
        // TextMesh near the cube. This used to instruct the user to look at the cube and count the test progression.
        private TextMesh textMesh;
        // Datastore is where the final results of the tests are pushed.
        private SphereDataStorage datastore;
        // The randomly generated polar angle of the sphere is stored here until the value is stored to the data storage.
        private float polarAngle;
        // Sphere object currently in the scene. Only one can exist at the time.
        private GameObject sphere;
        // The initial distance between the cube and the spheres.
        private float distance;
        // Z = Distance between viewver and the cube.
        private float z;
        private ShowText showText;
        private bool helpShown = false;

        List<float> angleList;

        public TestState(GameObject gameObject, SphereDataStorage datastore) : base(gameObject)
        {
            showText = GameObject.Find("InfoText").gameObject.GetComponent<ShowText>() as ShowText;
            sphereCount = 0;
            this.datastore = datastore;
            sphere = null;
            z = parent.transform.position.z;
            float startAngle = Mathf.Deg2Rad * NumberSettings.SpawnAngle;
            distance = z * Mathf.Tan(startAngle);
            textMesh = gameObject.transform.Find("InfoText").gameObject.GetComponent<TextMesh>() as TextMesh;
            angleList = GetRandomAngleList(NumberSettings.TestLength);
        }

        /// <summary>
        /// If user stops looking at the cube, the test will pause and state machine will return to the <c>State.IDLE</c>.
        /// If user has already clicked all the spheres, the test will end and state machine will proceed to the <c>State.SAVE</c>
        /// Otherwise the state machine will stay in this state.
        /// </summary>
        /// <returns></returns>
        public override State getNextState()
        {
            State newState = State.TEST;
            bool eyeContact = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, 100);
            if(!eyeContact){
                // User does not look at the box --> Destroy the sphere and go to the idle/wait state
                destroySphere();
                newState = State.IDLE;
                helpShown = (sphereCount > 0);
            }else if(sphereCount == NumberSettings.TestLength){
                // Test is completed --> Destroy the sphere, change the string and move to the Saving state.
                destroySphere();
                textMesh.text = STRINGS.DONE;
                newState = State.SAVE;
            }else{
                newState = State.TEST;
            }
            return newState;
        }

        /// <summary>
        /// The real test is ran in this function.
        /// If the user has just started a new test, this method will start a coroutine (<c>ShowTextForSeconds</c>) 
        /// which will show the test instructions for 3 seconds before the test actually starts.
        /// 
        /// If there are no sphere present, ie. previous sphere was clicked away or user just started the test, the function will
        /// wait until instruction text has been hidden and then spawns a new sphere in the queue.
        /// 
        /// If user clicks a button, the sphere will be removed, the measurements are recorded and a text box is shown user for brief moment
        /// to indicate how long test has progressed.
        /// 
        /// TODO: The blinking text box between sphere spawns might become REALLY obnoxious quite fast. Further user testing is required.
        /// </summary>
        public override void updateState()
        {
            if (sphereCount == 0 && !helpShown){
                showText.ShowTextForSeconds(NumberSettings.Direction == 1 ? STRINGS.CLICK_WHEN_SEE : STRINGS.CLICK_WHEN_NO_SEE, 3.0f);
                helpShown = true;
            }
            if(sphere == null && textMesh.text == ""){
                Vector3 position = getRandomSpawnPoint();
                sphere = Object.Instantiate(Resources.Load("LineSphere"), position, Quaternion.identity) as GameObject;
            }else if(OVRInput.GetUp(OVRInput.Button.Any) || Input.GetKeyDown("space")){
                updateMeasurements();
                destroySphere();
                showText.ShowTextForSeconds($"{sphereCount}/{angleList.Count}", 0.25f);
            }
        }

        /// <summary>
        /// The spheres will not actually spawn in random locations, but their locations are pre-determined.
        /// What is random is the order in which they are spawned. This function fetches the next value from
        /// the shuffled list of pre-determined values.
        /// 
        /// So the function generates the new location based on polar angle found from the shuffled list and
        /// distance determined in <c>distance</c>
        /// </summary>
        /// 
        /// <returns>The spawn or destination of the sphere as a <c>Vector3</c> object.</returns>
        /// <see cref="GetRandomAngleList" />
        Vector3 getRandomSpawnPoint()
        {
            // The initial value is a bogus value. Maybe it should be zero or something?
            float randomValue = Random.value;
            if(angleList.Count > sphereCount){
                randomValue = angleList[sphereCount];
            }
            polarAngle = randomValue * 2 * Mathf.PI;
            float x = Mathf.Cos(polarAngle) * distance;
            float y = Mathf.Sin(polarAngle) * distance;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Destroyes the sphere if it exists.
        /// </summary>
        private void destroySphere()
        {
            if (sphere != null){
                Object.Destroy(sphere);
                sphere = null;
            }
        }

        /// <summary>
        /// This method records the current location of the spehere, calculates it's polar coordinates 
        /// from user's view point and adds the result into the datastore. The method will also
        /// increace the test's progress.
        /// </summary>
        void updateMeasurements()
        {
            if(sphere == null){
                Debug.Log("Sphere does not exist!");
                Application.Quit();
                return;
            }
            Vector3 sphereLocation = sphere.transform.position;
            // Calculate angle between camera2sphere and camera2cube
            float radius = Vector3.Distance(sphereLocation, parent.gameObject.transform.position);
            Vector3 cameraLocation = Camera.main.transform.position;
            Vector3 camera2sphere = cameraLocation - sphereLocation;
            Vector3 camera2box = cameraLocation - parent.transform.position;
            float fovAngle = Vector3.Angle(camera2sphere, camera2box);
            // Add the measurement to the list
            datastore.addMeasurement(polarAngle, radius, fovAngle);
            ++sphereCount;
        }

        /// <summary>
        /// Generates a uniformyl distributed list of polar angles around the cube. The angle between the samples
        /// is same and it's determined by the number of spheres in the test. Then the list shuffled. 
        /// </summary>
        /// <param name="testLength">How many samples list should have</param>
        /// <returns>List of angles used in the test</returns>
        /// <remarks>
        /// In python this would look something like
        /// <code>
        /// deltaAngle = 2*PI / testLength
        /// myList = [x*deltaAngle for x in range(testLength)]
        /// myList.shuffle()
        /// return myList
        /// </code>
        /// But because C# lists doesn't have python like inline list constructors or 
        /// native shuffle method, we have to go for more verbose route.
        /// </remarks>
        static List<float> GetRandomAngleList(int testLength)
        {
            float n = testLength;
            var angleList = new List<float>();
            float angle = 0;
            for (int i = 0; i < testLength; ++i)
            {
                angle += 1 / n;
                angleList.Add(angle);
            }
            System.Random rng = new System.Random();
            while (testLength > 1)
            {
                --testLength;
                int k = (int)rng.Next(testLength + 1);
                float value = angleList[k];
                angleList[k] = angleList[testLength];
                angleList[testLength] = value;
            }
            return angleList;
        }
    }
}