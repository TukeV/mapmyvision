﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MapMyVision.Assets.Scripts;
using UnityEngine.UI;
using System;

/// <summary>
/// Controls the menu which opened when user wants to start the "Disappearing Spheres" -test.
/// </summary>
public class DisappearingSettings : MonoBehaviour
{
    /// <summary>
    /// These UI elements are created in the <c>MainMenu</c> scene and they are binded to these variables in the Unity Editor.
    /// </summary>
    public GameObject mainPanel;
    public GameObject disappearPanel;
    
    public Slider LengthSlider;
    public Text LengthValueText;
    
    public Slider SpeedSlider;
    public Text SpeedValueText;

    public Slider SizeSlider;
    public Text SizeValueText;
    
    // Start is called before the first frame update
    void Start()
    {
        // Set UI to it's default state when the scene begins.
        SetDefaults();
    }

    /// <summary>
    /// Fetches the default values from <c>PlayerPrefs</c> storage and set's slider's to their values.
    /// </summary>
    void SetDefaults(){
        float testLength = (float) PlayerPrefs.GetInt("DisappearTestLength", 100);
        float speed =  PlayerPrefs.GetFloat("DisappearSpeed", 1.2f);
        float size =  PlayerPrefs.GetFloat("DisappearSize", 0.2f);
        
        LengthSlider.value = testLength;
        // While the speed and size values are floats, the slider actually uses
        // integers to make it appear snappy. 
        // The values on the slider ar 10 times larger than the actual values.
        SpeedSlider.value =  (int) Math.Round(speed * 10);
        SizeSlider.value =  (int) Math.Round(size * 10);
        // Sometimes labels fail to update, so we have to do it manually.
        UpdateLabels();
    }

    /// <summary>
    /// Updates all the labels by calling their update functions.
    /// </summary>
    void UpdateLabels(){
        OnLengthUpdated();
        OnSpeedUpdated();
        OnSizeUpdated();
    }

    /// <summary>
    /// Updates PlayerPreferences to the values given in parameters.
    /// </summary>
    /// <param name="testLength">How long the new default test will be.</param>
    /// <param name="speed">The speed of the sphere in degrees per second.</param>
    /// <param name="size">The size of the sphere.</param>
    void UpdatePreferences(int testLength, float speed, float size){
        PlayerPrefs.SetInt("DisappearTestLength", testLength);
        PlayerPrefs.SetFloat("DisappearSpeed", speed);
        PlayerPrefs.SetFloat("DisappearSize", size);
    }

    /// <summary>
    /// Sends the values to the Sphere Test Scene.
    /// </summary>
    /// <param name="testLength">How many sphere's there will be in the test.</param>
    /// <param name="speed">The speed of the sphere in degrees per second.</param>
    /// <param name="size">The size of the sphere.</param>
    /// <remark>Because these parameters cannot be sent to the test scene by other means, they are stored in the static class.</remark>
    void Copy2NumberSettings(int testLength, float speed, float size){
        NumberSettings.TestLength = testLength;
        NumberSettings.Speed = speed;
        NumberSettings.Size = size;
        NumberSettings.SpawnAngle = 50f;
        NumberSettings.Direction = 0;
    }

    /// <summary>
    /// Reads the settings from the UI, stores them to the <c>PlayerPrefs</c> before sending them 
    /// to the <c>NumberSettings</c> before starting the <c>SphereTestScene</c>.
    /// </summary>
    
    public void OnStartClicked(){
        int testLength = (int) LengthSlider.value;
        // The speed and size slider values are 10 times larger, so they need to be divided by 10.
        float speed = (float) Math.Round(SpeedSlider.value * 0.1f, 1);
        float size = (float) Math.Round(SizeSlider.value * 0.1f, 1);
        UpdatePreferences(testLength, speed, size);
        Copy2NumberSettings(testLength, speed, size);
        SceneManager.LoadScene("SphereTestScene", LoadSceneMode.Single);
    }

    /// <summary>
    /// Resets the UI elements to their default values.
    /// </summary>
    public void OnDefaultClicked(){
        SetDefaults();
    }

    /// <summary>
    /// When Back button (the UI element) is clicked, the current panel is hidden and the main menu is set back to active.
    /// </summary>
    public void OnBackClicked(){
        mainPanel.SetActive(true);
        disappearPanel.SetActive(false);
    }

    /// <summary>
    /// Callback which updates <c>LengthSliderText</c> when <c>LengthSlider</c> is updated.
    /// </summary>
    public void OnLengthUpdated(){
        LengthValueText.text = LengthSlider.value.ToString();
    }

    /// <summary>
    /// Callback which updates the <c>SpeedValueText</c> when <c>SpeedSlider</c> is updated.
    /// </summary>
    public void OnSpeedUpdated(){
        // Slider values are ten times larger so they need to be divided by 10.
        float value = (float) Math.Round(SpeedSlider.value * 0.1f, 1);
        SpeedValueText.text = $"{value} deg/s";
    }

    /// <summary>
    /// Updates the label after slider value has been changed.
    /// </summary>
    public void OnSizeUpdated(){
        // Slider values are ten times larger so they need to be divided by 10.
        float value = (float) Math.Round(SizeSlider.value * 0.1f, 1);
        SizeValueText.text = $"{value}";
    }

    public void OnBrightnessUpdated(){
        // TODO
    }

    // Update is called once per frame
    void Update()
    {
        // If this panel is active and the back button on the controller is pressed, 
        // the effect will be the same as the UI button is pressed.
        if (disappearPanel.activeSelf && OVRInput.GetUp(OVRInput.Button.Back)){
            OnBackClicked();
        }
    }
}
