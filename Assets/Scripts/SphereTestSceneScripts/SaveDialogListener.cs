﻿using System;
using System.Collections;
using System.Collections.Generic;
using MapMyVision.Assets.Scripts;
using MapMyVision.Assets.Scripts.StateMachine;
using UnityEngine;

public class SaveDialogListener : MonoBehaviour
{
    /// <summary>
    /// An instance of this class is bound to the save dialog shown after the test. 
    /// It will call saveHandler depending on the user's answer to the save prompt.
    /// </summary>
    SaveHandler saveHandler;
    public SaveHandler SaveState {
        set{ 
            Debug.Log("SaveHandler Set");
            saveHandler = value;
        }
    }
    void Start(){}

    public void save(){
        if(saveHandler == null){
            saveHandler = GameObject.Find("Cube").GetComponent<CubeActivation>().saveState;
        }
        Debug.Log("Save called");
        saveHandler?.DoSave();
    }

    public void nosave(){
        if(saveHandler == null){
            saveHandler = GameObject.Find("Cube").GetComponent<CubeActivation>().saveState;
        }
        Debug.Log("Don't save called");
        saveHandler?.DoNotSave();
    }
}
