namespace MapMyVision.Assets.Scripts
{
    public struct PolarData
    {
        /// <summary>
        /// This struct is used to describe individual samples gathered during a testing process.
        /// </summary>
        /// <value><c>polar</c> is the polar coordinate of the sample. </value>
        /// <value><c>radius</c> is the in-game distance between the center point (where the box lies) and the sample.</value>
        /// <value>
        /// <c>fov</c> represents the user's field of vision in direction of the polar coordinate. 
        /// The angle is calculated by taking te angle between two vectors, which both start from center of the user's view port, 
        /// but first of them goes trough the box lying in front of the user and other goes trough the location
        /// where user saw sphere to appear or disappear.
        /// </value>
        public float polar, radius, fov;
        
        public PolarData(float p, float r, float f){
            polar = p;
            radius = r;
            fov = f;
        }
    }
}